ARG BASE_NAME=qm-workload
ARG IMAGE_TAG=latest
ARG DOCKER_REGISTRY=localhost

ARG DIGITAL_COCKPIT_USER=digital-cockpit
ARG DIGITAL_COCKPIT_HOME=/var/lib/digital-cockpit

FROM $DOCKER_REGISTRY/$BASE_NAME:$IMAGE_TAG AS base

ARG DIGITAL_COCKPIT_USER
ARG DIGITAL_COCKPIT_HOME

RUN dnf -y install qt6-qt5compat                                        \
                   qt6-qtwebsockets                                     \
                   qt6-qtbase                                           \
                   qt6-qtquick3d                                        \
                   qt6-qtshadertools                                    \
                   qt6-qtdeclarative                                    \
                   qt6-qtquick3d                                        \
    && dnf -y install git                                               \
                      cmake make gcc gcc-c++                            \
                      qt6-qt5compat-devel                               \
                      qt6-qtwebsockets-devel                            \
                      qt6-qtbase-devel                                  \
                      qt6-qtbase-private-devel                          \
                      qt6-qtquick3d-devel                               \
                      qt6-qtshadertools-devel                           \
                      qt6-qtdeclarative-devel                           \
                      qt6-qtquick3d-devel                               \
    # Build QTmqtt
    && git clone --depth 1 --branch 6.6.0                                     \
                 http://code.qt.io/qt/qtmqtt.git                              \
    && cd qtmqtt                                                              \
    && cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr .                               \
    && make -j4                                                               \
    && make install                                                           \
    && cd .. && rm -fr qtmqtt                                                 \
    # Build qtquickdesigner-components
    && git clone --depth 1 --branch dev                                       \
                 https://github.com/qt-labs/qtquickdesigner-components.git    \
    && cd qtquickdesigner-components                                          \
    && cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr .                               \
    && make                                                                   \
    && make install                                                           \
    && cd .. && rm -fr qtquickdesigner-components                             \
    && dnf -y remove git                                               \
                     cmake make gcc gcc-c++                            \
                     qt6-qt5compat-devel                               \
                     qt6-qtwebsockets-devel                            \
                     qt6-qtbase-devel                                  \
                     qt6-qtbase-private-devel                          \
                     qt6-qtquick3d-devel                               \
                     qt6-qtshadertools-devel                           \
                     qt6-qtdeclarative-devel                           \
                     qt6-qtquick3d-devel                               \
    && dnf clean all && rm -rf /var/cache/dnf